package com.seleniumexpress;

import com.seleniumexpress.ioc.College;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class XmlApp {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        System.out.printf("Context loaded");
        College bmCollage = context.getBean("bmCollege", College.class);
        bmCollage.collegeInfo();
    }
}
