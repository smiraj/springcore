package com.seleniumexpress;

import com.seleniumexpress.config.AppConfig;
import com.seleniumexpress.events.publisher.BringPublisher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AnnotationApp {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        BringPublisher bring = context.getBean("bringe", BringPublisher.class);
        bring.broadcastBigBangTheory();
        bring.broadcastGameOfThrone();
    }
}
