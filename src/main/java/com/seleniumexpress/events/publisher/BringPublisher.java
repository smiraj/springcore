package com.seleniumexpress.events.publisher;

import com.seleniumexpress.events.series.GameOfThroneSeries;
import com.seleniumexpress.events.series.TheBigBangTheorySeries;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component("bringe")
public class BringPublisher {

    private final ApplicationEventPublisher applicationEventPublisher;

    public BringPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public void broadcastBigBangTheory(){
        System.out.println("Starting Big Bang Theory");
        applicationEventPublisher.publishEvent(new TheBigBangTheorySeries(this, "Ep-001"));
    }

    public void broadcastGameOfThrone(){
        System.out.println("Starting Game of Throne");
        applicationEventPublisher.publishEvent(new GameOfThroneSeries("Ep-002"));
    }
}
