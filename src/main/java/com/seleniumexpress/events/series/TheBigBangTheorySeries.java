package com.seleniumexpress.events.series;

import org.springframework.context.ApplicationEvent;

public class TheBigBangTheorySeries extends ApplicationEvent {

    private String episodeNumber;

    public TheBigBangTheorySeries(Object source) {
        super(source);
    }

    public TheBigBangTheorySeries(Object source, String episodeNumber) {
        super(source);
        this.episodeNumber = episodeNumber;
    }

    public String getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(String episodeNumber) {
        this.episodeNumber = episodeNumber;
    }
}
