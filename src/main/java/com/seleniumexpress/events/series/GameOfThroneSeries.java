package com.seleniumexpress.events.series;

public class GameOfThroneSeries {

    private String episodeNumber;

    public GameOfThroneSeries(String episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public String getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(String episodeNumber) {
        this.episodeNumber = episodeNumber;
    }
}
