package com.seleniumexpress.events.listener;

import com.seleniumexpress.events.series.TheBigBangTheorySeries;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class NetflixListener implements ApplicationListener<TheBigBangTheorySeries> {

    private void listeningTheBigBangTheoryEvent(String episode){
        System.out.println("NetflixListener: Listening The Big Bang theory : " + episode);
    }

    public void onApplicationEvent(TheBigBangTheorySeries event) {
        listeningTheBigBangTheoryEvent(event.getEpisodeNumber());
    }
}
