package com.seleniumexpress.events.listener;

import com.seleniumexpress.events.series.GameOfThroneSeries;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class Zee5Listener {

    @EventListener
    private void listeningGameOfThroneEvent(GameOfThroneSeries series){
        System.out.println("Zee5Listener: Listening Game of Throne : " + series.getEpisodeNumber());
    }
}
