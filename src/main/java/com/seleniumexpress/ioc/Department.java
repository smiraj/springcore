package com.seleniumexpress.ioc;

import org.springframework.beans.factory.annotation.Value;

public class Department {

    @Value("${department.name}")
    private String name;

    @Value("${department.numberOfStudent}")
    private int numberOfStudent;

    public void departmentInfo(){
        System.out.println("Department name: " + name);
        System.out.println("Number of students: " + numberOfStudent);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfStudent() {
        return numberOfStudent;
    }

    public void setNumberOfStudent(int numberOfStudent) {
        this.numberOfStudent = numberOfStudent;
    }
}
