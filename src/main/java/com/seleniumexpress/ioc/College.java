package com.seleniumexpress.ioc;

public class College {

    private Department department;

    private CollegeFunction collegeFunction;

    public  void collegeInfo(){
        department.departmentInfo();
        collegeFunction.admissionFunctionality();
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public void setCollegeFunction(CollegeFunction collegeFunction) {
        this.collegeFunction = collegeFunction;
    }
}
